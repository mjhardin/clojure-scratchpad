# Clojure Scratchpad

This is my scratchpad of projects as I go through the book "Clojure for the Brave and True".

## Projects

* clojure-noob - The actual scratchpad
* fwpd - Retyped code from chapter 4
* the-divine-cheese-code - Retyped code from chapter 6
