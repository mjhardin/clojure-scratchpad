# Clojure Scratchpad

This is my scratchpad as I go through the book "Clojure for the Brave and True"

The code in this folder is from me working through the exercises at the end of most chapters. In most cases I haven't attempted to go through the code and find more efficient ways to write a function.


