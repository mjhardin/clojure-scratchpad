(ns clojure-noob.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "I'm a little teapot!"))

(defn -train
  []
  (println "Choo choo!"))


(defn error-message
  [severity]
  (str "OH NO! "
       (if (= severity :mild)
         "KIND OF BROKEN BUT STILL WORKING! YAY!"
         "IT'S ALL BURNING DOWN!"))
)

(defn too-enthusiastic
  "Return a cheer that might be a bit too enthusiastic"
  [name]
  (str "OH. MY. GOD! " name " YOU ARE MOST DEFINITELY LIKE THE BEST MAN SLASH WOMAN EVER I LOVE YOU AND WE SHOULD RUN AWAY SOMEWHERE "
))

(defn no-params
  []
  "I take no parameters!")

(defn one-param
  [x]
  (str "I take one parameter: " x))

(defn two-params
  [x y]
  (str "Two parameters! That's nothing! Pah! I will smoosh them together to spite you! " x y))

(defn x-chop
  "Describe the kind of chop you're inflicting on someone"
  ([name chop-type]
   (str "I " chop-type " chop " name "! Take that!"))
  ([name]
   (x-chop name "karate"))
)

(defn weird-arity
  ([]
   "Destiny dressed you this morning, my friend, and now Fear is trying to pull off your pants. If you give up, if you give in, you're gonna end up naked with Fear just standing there laughing at your dangling unmentionables! - the Tick")
  ([number]
   (inc number))
)

(defn codger-talk
  [kid]
  (str "Get off my lawn, " kid "!!!!"))

(defn codger
  [& kids]
  (map codger-talk kids))

(defn favorite-things
  [name & things]
  (str "Hi, " name ". " (clojure.string/join ", " things)
       ": these are a few of my favorite things.")
)

;; Return the first element of a collection
(defn my-first
  [[first-thing]]
  first-thing)

(defn chooser
  [[first-choice second-choice & unimportant-choices]]
  (println (str "Your first choice is: " first-choice))
  (println (str "Your second choice is: " second-choice))
  (println (str "Who cares what your first choices were. (Actually, we do. Here they are: " (clojure.string/join ", " unimportant-choices)))
)

(defn announce-treasure-location
  [{:keys [lat lng]}]
  (println (str "Treasure lat: " lat))
  (println (str "Treasure lng: " lng))
)

(defn illustrative-function
  []
  (+ 1 304)
  30
  "joe"
)

(defn number-comment
  [x]
  (if (> x 6)
    "Bigger than 6; big freakin' deal."
    "For shame.")
)

(defn inc-maker
  "Create a custom incrementor"
  [inc-by]
  #(+ % inc-by)
)

(defn dec-maker
  "Create a custom decrementor"
  [dec-by]
  #(- % dec-by)
)

(defn mapset
  "Create a set after mapping to a function"
  [function list]
  (def _list (map function list))
  (set _list)
)

(defn add100
  "Number incrementor. v2"
  [number]
  (+ number 100)

)

;; (defn addRandomNumber
;;   "Number incrementor. Chooses a random number and adds it to the inputted integer."
;;   [number choices]
;;   (loop [iter choices num_list []]
;;     (if (= 0 iter)
;;       num_list
;;       (recur             
;;        (into num_list
;;              (dec iter)
             
;;              (set (* iter 10))
;;              ))
;;       )
;;     )
;; )
;; Chapter 3
(def inc3 (inc-maker 100))


(def asym-hobbit-body-parts [
                             {:name "head" :size 3}
                             {:name "left-eye" :size 1}
                             {:name "left-ear" :size 1}
                             {:name "mouth" :size 1}
                             {:name "nose" :size 1}
                             {:name "neck" :size 2}
                             {:name "left-shoulder" :size 3}
                             {:name "left-upper-arm" :size 3}
                             {:name "chest" :size 10}
                             {:name "back" :size 10}
                             {:name "left-forearm" :size 3}
                             {:name "abdomen" :size 6}
                             {:name "left-kidney" :size 1}
                             {:name "left-hand" :size 2}
                             {:name "left-knee" :size 2}
                             {:name "left-thigh" :size 4}
                             {:name "left-lower-leg" :size 3}
                             {:name "left-achilles" :size 1}
                             {:name "left-foot" :size 2}
])

(defn matching-part
  [part]
  {:name (clojure.string/replace (:name part) #"^left-" "right-")
   :size (:size part)}
)

(defn multiply-part
  [part number]
  (if (clojure.string/starts-with? (:name part) "left-")
    (loop [appendage-number number body-parts []]
      (if (= appendage-number 0)
        body-parts
        (recur 
         (dec appendage-number) 
         (into body-parts 
               (set [{
                      :name (clojure.string/replace  (clojure.string/join [(:name part) "-" appendage-number]) #"^left-|^right-" "")
                      :size (:size part)}]))))
      )
    [part]
    )
)

(defn redundant-part
  [part number]
  {:name (clojure.string/join [(:name part) number])
   :size (:size part)}
)

(defn symmetrize-body-parts
  "Expects a seq of maps that have a :name and :size"
  [asym-body-parts]
  (loop [remaining-asym-parts asym-body-parts final-body-parts []]
    (if (empty? remaining-asym-parts)
      final-body-parts ;; the output is in the middle!
      (let [[part & remaining] remaining-asym-parts]
        (recur remaining ;; "remaining" is the new "asym-body-parts" on next loop
               (into final-body-parts ;; eventually will be the output
                     (set [part (matching-part part)])) ;; where the work gets done
               )
        )
      )
    )
)

(defn better-symmetrize-body-parts
  "Expects a seq of maps that have a :name and :size"
  [asym-body-parts]
  (reduce ;; reduce performs the same function on every part of an iterable 
   (fn [final-body-parts part] ;; this anonymous function takes two args
            (into final-body-parts (set [part (matching-part part)]))) ;; into is the "output"
   [] asym-body-parts) ;; the empty vector is the starting value asym-body-parts is the iterable
)

(defn expand-body-parts
 "Expects a seq of maps that have a :name and :size and a number to create extra appendages."
 [body-parts number-of-appendages]
 (reduce (fn [final-body-parts part] (into final-body-parts (multiply-part part number-of-appendages)))
         [] body-parts
))

(defn hit
  [asym-body-parts]
  (let [sym-parts (better-symmetrize-body-parts asym-body-parts)
        body-part-size-sum (reduce + (map :size sym-parts))
        target (rand body-part-size-sum)]
    (loop [[part & remaining] sym-parts
           accumulated-size (:size part)]
      (if (> accumulated-size target)
        part
        (recur remaining (+ accumulated-size (:size (first remaining))))))
    )
)
;; Chapter 4
(reduce (fn [new-map [key val]]
          (assoc new-map key (inc val)))
        {}
        {:max 40 :min 10})

(def human-consumption [8.1 7.3 6.6 5.0])

(def critter-consumption [0.0 0.2 0.3 1.1])

(defn unify-diet-data
  [human critter]
  {:human human
   :critter critter})

(def identities
  [{:alias "Batman" :real "Bruce Wayne"}
   {:alias "The Truth" :real "Paul Pierce"}
   {:alias "The One" :real "Neo"}
   {:alias "Spider-Man" :real "Peter Parker"}])

(reduce (fn [new-map [key val]]
          (if (> val 4)
            (assoc new-map key val)
            new-map))
        {}
        {:human 1
         :critter 3.9})

(def food-journal
  [{:month 1 :day 1 :human 1.3 :critter 2.3}
   {:month 1 :day 2 :human 3.3 :critter 56.3}
   {:month 2 :day 1 :human 5.3 :critter 2.3}
   {:month 2 :day 2 :human 0.3 :critter 1.3}
   {:month 3 :day 1 :human 6.3 :critter 2.3}
   {:month 4 :day 1 :human 5.3 :critter 2.3}
   {:month 5 :day 1 :human 5.3 :critter 2.3}])

(def vampire-database
  {0 {:makes-blood-puns? false :has-pulse? true :name "McFishwich"}
   1 {:makes-blood-puns? false :has-pulse? true :name "McMackson"}
   2 {:makes-blood-puns? true :has-pulse? false :name "Damon Salvatore"}
   3 {:makes-blood-puns? true :has-pulse? true :name "Mickey Mouse"}})

(defn vampire-related-details
  [social-security-number]
  (Thread/sleep 1000)
  (get vampire-database social-security-number))

(defn vampire?
  [record]
  (and (:makes-blood-puns? record)
       (not (:has-pulse? record))
       record))

(defn identify-vampire
  [social-security-numbers]
  (first (filter vampire?
                 (map vampire-related-details social-security-numbers))))

(def not-vampire? (complement vampire?))

(defn identify-human
  [social-security-numbers]
  (filter not-vampire?
          (map vampire-related-details social-security-numbers)))


(concat (take 8 (repeat "na")) ["Batman!"])

(take 3 (repeatedly (fn [] (rand-int 10))))

(defn even-numbers
  ([] (even-numbers 0))
  ([i] (cons i (lazy-seq (even-numbers (+ i 2))))))

(take 10 (even-numbers))

(defn my-partial
  [partialized-fn & args]
  (fn [& more-args]
    (apply partialized-fn (into args more-args))))

(defn lousy-logger
  [log-level message]
  (condp = log-level
    :warn (clojure.string/lower-case message)
    :emergency (clojure.string/upper-case message)))

(defn my-complement
  [fun]
  (fn [& args]
    (not (apply fun args))))

;; Chapter 5

(defn two-comp
  [f g]
  (fn [& args]
    (f (apply g args))))

(defn my-comp
  [f & g]
  (if (empty? g)
    (fn [& args] (apply f args))
    (recur (fn [& args]
             (f (apply (first g) args))) (rest g))))

(defn test-empty-destruct
  [f & g]
  (if (empty? g)
    (println f)
    (recur (str f (first g)) (rest g))))

(defn sum
  ([vals] (sum vals 0))
  ([vals accumulating-total]
   (if (empty? vals)
     accumulating-total
     (recur (rest vals) (+ (first vals) accumulating-total)))))

(require '[clojure.string :as s])
(defn clean
  [text]
  (s/replace (s/trim text) #"lol" "LOL"))


(def character
  {:name "Smooches McCutes"
   :attributes {:intelligence 10
                :strength 4
                :dexterity 5}})

(defn attr
  "Retrieve any of a characters attributes."
  [character attribute]
  (get-in character [:attributes attribute]))

(def c-int (comp :intelligence :attributes))
(def c-str (comp :string :attributes))
(def c-dex (comp :dexterity :attribuets))

;(c-int character)

(def inc3 (inc-maker 3))

(defn spell-slots
  [char]
  (int (inc3 (/ (c-int char) 2))))

(def spell-slots-comp (comp int inc3 #(/ % 2) c-int))

(spell-slots-comp character)

(defn sleepy-identity
  "Returns the given value after 1 second"
  [x]
  (Thread/sleep 1000)
  x)

(defn my-assoc-in
  "My implementation of Clojure's assoc function"
  [m [k & kvs] v]
  (if (empty? kvs)
    (assoc m k v)
    (recur {} (cons k (take (dec (count kvs)) kvs)) (assoc m (last kvs) v))))

(def this-map (assoc-in {}
                        [:k :v :vd :df :gf] 100))

(def add10 (inc-maker 10))

(update-in this-map [:k :v :vd :df :gf] add10)

(defn my-update-in
  "My implementatoin of Clojure's update-in function"
  [m [k & ks] f & args]
  (let [old-value (get-in m (cons k ks))]
    (my-assoc-in m (cons k ks) (apply f old-value args))))

(my-update-in this-map [:k :v :vd :dt :gf] (fn [& args]
                                          (if (empty? (first args))
                                            (add10 0)
                                            (add10 (first args)))))

;; Chapter 6


;; Chapter 7

(eval
 (let [infix (read-string "(1 + 1)")]
   (list (second infix) (first infix) (last infix))))

(defmacro ignore-last-operand
  [function-call]
  (butlast function-call))

(defn infix
  [infixed]
  (list (second infixed)
        (first infixed)
        (last infixed)))



;; (1 + 4 * 3 - 5)

(defn infixer
  "Infix function with correct order of operations for paren averse people."
  ([statements]
   (if (even? (count statements))
     (println "This is not a valid math equation.")
     (infixer () statements)))
  ([existing statements]
   (let [equation (take 3 statements) remainder (drop 3 statements)]
     (if (empty? remainder)
       (if (empty? existing)
         (eval (list (second equation) (first equation) (last equation)))
         (if (even? (count equation))
           (eval (list (first equation) existing (second equation)))
           (eval (list (second equation) existing (first equation) (last equation)))))
       (if (empty? existing)
         (if (contains? '{* /} (second equation))
           (recur (list (second equation)
                        (first equation) 
                        (last equation)) (remainder))
           (recur (list (second equation)
                        (first equation))
                  (cons (last equation) remainder)))
         (if (contains? '{* /} (second equation))
           (recur (concat
                   existing [(list (second equation) (first equation) (last equation))])
                  remainder)
           (if (number? (second equation))
             (recur (list (first equation)
                          existing
                          (second equation))
                    (cons (last equation) remainder))
             (recur (list (second equation)
                          existing
                          (first equation))
                    (cons (last equation) remainder))))))))
)

(macroexpand '(ignore-last-operand (+ 1 2 10)))

;; (infix (1 + 2))

(defn your-fave-film
  [level-of-interest fname film]
  (let [message
        (eval (list (read-string "str") fname " likes " film "."))]
    (condp = level-of-interest
      :hates (str (clojure.string/replace message #"likes" "hates") " :(")
      :likes message
      :loves (str (clojure.string/replace message #"likes" "loves") " :D")
      :really-loves (str (clojure.string/upper-case (clojure.string/replace (clojure.string/replace message #"likes" "really loves") #"\." "!")) "!!!"))))

(def likes (partial your-fave-film :likes))
(def hates (partial your-fave-film :hates))
(def loves (partial your-fave-film :loves))
(def really-loves (partial your-fave-film :really-loves))

;; Chapter 8

(defmacro my-print-whoopsie
  [expression]
  (list 'let ['result expression]
        (list 'println 'result)
        'result))

(macroexpand '(when (the-cows-come :home)
               (call me :pappy)
               (slap me :silly)))

(defn criticize-code
  [criticism code]
  `(println ~criticism (quote ~code)))

(defmacro code-critic
  "Phrases are courtest of Hermes Conrad from Futurama"
  [bad good]
  `(do ~(map #(apply criticize-code %) [["Great squid of Madrid, this is bad code:" bad]
                                        ["Sweet gorilla of Manila, this is good code:" good]])))
(def message "Good job!")
(defmacro without-mischief
  [& stuff-to-do]
  (let [macro-message (gensym 'message)]
    `(let [~macro-message "Oh, big deal!"]
       ~@stuff-to-do
       (println "I still need to say: " ~macro-message))))


(;without-mischief 
 ;(println "Here's how I feel about that thing you did:" message)
 )

(defmacro report
  [to-try]
  `(let [result# ~to-try]
    (if result#
       (println (quote ~to-try) "was successful:" result#)
       (println (quote ~to-try) "was not successful:" result#))))

(defmacro doseq-macro
  [macroname & args]
  `(do 
     ~@(map (fn [arg] (list macroname arg)) args)))

(def order-details
  {:name "Jeff Boxworthy"
   :email "jeffboxworthy@gmail.com"})

(def order-detail-validations
  {:name
   ["Please enter a name" not-empty]
   :email
   ["Please enter an email address" not-empty
    "Your email address doesn't look like an email address" #(or (empty? %) (re-seq #"@" %))]})

(defn error-messages-for
  "Return a seq of error messages"
  [to-validate message-validator-pairs]
  (map first (filter #(not ((second %) to-validate))
                     (partition 2 message-validator-pairs))))

(defn validate
  "Returns a map with a vector of errors for each key"
  [to-validate validations]
  (reduce (fn [errors validation]
            (let [[fieldname validation-check-groups] validation
                  value (get to-validate fieldname)
                  error-messages (error-messages-for value validation-check-groups)]
              (if (empty? error-messages)
                errors
                (assoc errors fieldname error-messages))))
          {}
          validations))

(def my-errors {:failure "this is bad"})

(defmacro if-valid
  "Handle validation more concisely"
  [to-validate validations errors-name & then-else]
  `(let [~errors-name (validate ~to-validate ~validations)]
     (if (empty? ~errors-name)
       ~@then-else)))

;; Exercises

(defmacro when-valid
  "Handle validation and 'do' things if the data validates"
  [to-validate validations errors-name & then]
  `(let [~errors-name (validate ~to-validate ~validations)]
     (when (empty? ~errors-name)
       ~@then)))

(defmacro defattrs
  "Define attribute getters"
  ([fn-name attr]
   `(def ~fn-name (comp ~attr :attributes)))
  ([fn-name attr & attrs]
   `(do 
      (defattrs ~fn-name ~attr)
      (defattrs ~@attrs)))
  )

; Chapter 9

;; (let [result (future (Thread/sleep 3000)
;;                      (+ 1 1))]
;;   (println "deref: " (deref result))
;;   (println "It will be at least 3 seconds before I print"))

;; (deref (future (Thread/sleep 1000) 0) 10 5)

(defmacro wait
  "Sleep `timeout` seconds before evaluating body"
  [timeout & body]
  `(do (Thread/sleep ~timeout) ~@body))

;; (time (let [saying3 (promise)]
;;         (future (deliver saying3 (wait 100 "Cheerio!")))
;;         @(let [saying2 (promise)]
;;            (future (deliver saying2 (wait 400 "Pip pip!")))
;;            @(let [saying1 (promise)]
;;               (future (deliver saying1 (wait 200 "'Ello, gov'na!")))
;;               (println @saying1)
;;               saying1)
;;            (println @saying2)
;;            saying2)
;;         (println @saying3)
;;         saying3))

(defmacro enqueue
  ([q concurrent-promise-name concurrent serialized]
   `(let [~concurrent-promise-name (promise)]
      (future (deliver ~concurrent-promise-name ~concurrent))
      (deref ~q)
      ~serialized
      ~concurrent-promise-name))
  ([concurrent-promise-name concurrent serialized]
   `(enqueue (future) ~concurrent-promise-name ~concurrent ~serialized)))

;; (time @(-> (enqueue saying (wait 200 "'Ello, gov'na!") (println @saying))
;;            (enqueue saying (wait 400 "Pip pip!") (println @saying))
;;            (enqueue saying (wait 100 "Cheerio!") (println @saying))))

(defmacro search-encode
  "Adds `+` symbol in place of space"
  [string]
  `(clojure.string/replace ~string (re-pattern "\\s+") "+"))

(def search-engines {:yahoo
                     {:path "https://search.yahoo.com/search?p="
                      :file "yahoo.html"}
                     :bing
                     {:path "http://www.bing.com/search?q="
                      :file "bing.html"}})

(defn execute-search
  "Execute a search on a search engine"
  [engine qry]
  (println (str "Searching " engine))
  (slurp (str (:path ((keyword engine) search-engines)) (search-encode qry)))
  )

(defn better-search
  ([qry engines]
   (map 
    (fn 
      [k] 
      (enqueue search (execute-search k qry) 
               (do
                 (spit (:file (k search-engines)) @search)
                 (println (str "Done searching " k)))))
    engines))
  ([qry]
   (better-search qry (keys search-engines)))
)

(defn gsearch
  "Search google"
  [qry]
  (let [b_results (future (execute-search :bing qry))]
    (println "Searching Bing...")
    (let [y_results (future (execute-search :yahoo qry))]
       (println "Searching Yahoo...")
       (spit "yahoo.html" @y_results)
       (println "Done searching Yahoo..."))
    (spit "bing.html" @b_results)
    (println "Done searching Bing..."))
  )

;; Chapter 10 
;; Scratch work

(def sock-variaties
  #{"darned" "argyle" "wool" "horsehair" "mulleted"
    "passive-aggressive" "striped" "polka-dotted"
    "athletic" "business" "power" "invisible" "gollumed"})

(defn sock-count
  [sock-variety count]
  {:variety sock-variety
   :count count})

(defn generate-sock-gnome
  "Create an initial sock gnome state with no socks"
  [name]
  {:name name
   :socks #{}})

(def sock-gnome (ref (generate-sock-gnome "Barumpharumph")))
(def dryer (ref {:name "The Good Dryer"
                 :socks (set (map #(sock-count % 2) sock-variaties))}))

(defn steal-sock
  [gnome dryer]
  (dosync
   (when-let [pair (some #(if (= (:count %) 2) %) (:socks @dryer))]
     (let [updated-count (sock-count (:variety pair) 1)]
       (alter gnome update-in [:socks] conj updated-count)
       (alter dryer update-in [:socks] disj pair)
       (alter dryer update-in [:socks] conj updated-count)))))

(defn similar-socks
  [target-sock sock-set]
  (filter #(= (:variety %) (:variety target-sock)) sock-set))

(defn sleep-print-update
  [sleep-time thread-name update-fn]
  (fn [state]
    (Thread/sleep sleep-time)
    (println (str thread-name ": " state))
    (update-fn state)))
(def counter (ref 0))
;;(future (dosync (commute counter (sleep-print-update 100 "Thread A" inc))))
;;(future (dosync (commute counter (sleep-print-update 150 "Thread B" inc))))

(def ^:dynamic *troll-thought* nil)
(defn troll-riddle
  [your-answer]
  (let [number "man meat"]
    (when (thread-bound? #'*troll-thought*)
      (set! *troll-thought* number))
    (if (= number your-answer)
      "TROLL: You can cross the bridge!"
      "TROLL: Time to eat you, succulent human!")))
;; (binding [*troll-thought* nil]
;;   (println (troll-riddle 2))
;;   (println "SUCCULENT HUMAN: Oooooh! The answer was" *troll-thought*))

(def alphabet-length 26)

(def letters (mapv (comp str char (partial + 65)) (range alphabet-length)))

(defn random-string
  "Returns a random string of specified length"
  [length]
  (apply str (take length (repeatedly #(rand-nth letters)))))

(defn random-string-list
  [list-length string-length]
  (doall (take list-length (repeatedly (partial random-string string-length)))))

;;(def orc-names (random-string-list 3000 7000))

(def quotes (atom {}))

(defn update-atom
  [a v]
  (let [no-author (clojure.string/replace v #"--.*$" "")]
    (reduce (fn [e word]         
              (swap! a (fn [val]
                         (update val word (fn [ov]
                                            (let [wc (count (re-seq (re-pattern word) no-author))]
                                              (if (contains? val word)
                                                (+ ov wc)
                                                wc)))))))
            [] (clojure.string/split no-author #"\s")
            ))
)

;; Exercises

(defn quote-word-count
  "Retrieve quotes -- count words"
  [number]
  (let [qs (take number (repeatedly #(future (slurp "http://www.braveclojure.com/random-quote"))))]
    (reduce
     (fn 
       [e f]
       @f
       (update-atom quotes @f)       
       )
     [] qs)
    )
  )

;; Player models
(def npc {:hp 40 :remaining_hp 15})
(def healer (update-in npc [:potions] (fn [ov] (set [{:name "healing" :strength 100 :count 2}]))))

;; Player instances
(def frank (ref npc))
(def stephanie (ref healer))

(defn apply-potion
  "Function for applying a potion for a character's potions. Should only consume as much as needed per character."
  ([healer player potion]
   (dosync
    (while (< (:remaining_hp @player) (:hp @player))
      (alter player update-in [:remaining_hp] (fn [e] (min (:hp @player) (:strength potion))))
      (if (> (:count potion) 1)
        (alter healer update-in [:potions] conj (update potion :count dec)))
      (alter healer update-in [:potions] disj potion)
      (println (str "Applied healing potion to player.")))
    ))
  )
