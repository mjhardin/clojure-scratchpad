(ns fwpd.core)
(def filename "suspects.csv")

(def vamp-keys [:name :glitter-index])

(defn str->int 
  [str]
  (Integer. str))

(defn containskeys
  [record]
  (and (contains? record :name)
       (contains? record :glitter-index)))

(def conversion {:name identity
                 :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversion vamp-key) value))

(defn parse
  "Convert a CSV into a row of columns"
  [string]
  (map #(clojure.string/split % #",")
       (clojure.string/split string #"\n")))

(defn validate
  "Validate that a map contains the supplied keys"
  [record]
  (and (contains? record :name)
       (contains? record :glitter-index)))

(defn mapify
  "Return a seq of maps like {:name \"Edward Cullen\" :glitter-index 10}"
  [rows]
  (map (fn [unmapped-row]
         (reduce (fn [row-map [vamp-key value]]
                   (assoc row-map vamp-key (convert vamp-key value)))
                 {}
                 (map vector vamp-keys unmapped-row)))
       rows))

(defn append
  "Append a map to a seq of maps and return the new seq. The seq to append is tested for whether all items contain the :name and :glitter-index key. The list only includes items up to improperly formatted records."
  [existing_map records]
  (concat existing_map (take-while #(validate %) records)))

(defn easy_append
  "Append a map to a seq of maps and return the new seq. The seq to be appended is filtered to only include properly formatted records"
  [existing_map records]
  (concat existing_map (filter #(validate %) records)))

(defn glitter-filter
  [minimum-glitter records]
  (into [] (map (fn [row] (get row :name))
                (filter #(>= (:glitter-index %) minimum-glitter) records))))

(defn csvize
  "Given a record parse a CSV"
  [seq]
  (clojure.string/join 
   "\n" 
   (into [] 
         (map 
          (fn [row] 
            (clojure.string/join "," [(get row :name) (get row :glitter-index)])) 
          seq)))
  )
